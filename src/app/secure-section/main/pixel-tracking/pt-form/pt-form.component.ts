import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';

import { MainStorageService } from '../../../services/main-storage.service';

import { Site } from '../../../../core/core.model';
import { PTEventData } from '../../../store/main.model';
import * as fromRoot from '../../../../app.reducers';
import * as UserActions from '../../../user/store/user.actions';

@Component({
  selector: 'eaf-pt-form',
  templateUrl: './pt-form.component.html',
  styleUrls: ['./pt-form.component.sass']
})
export class PtFormComponent implements OnInit, OnChanges {
  @Input() id: number;
  @Output() close = new EventEmitter<boolean>();
  ptEventsForm: FormGroup;
  ptEventsList = [];
  selectedPTEvent: {id: number, name: string};
  siteState: Site;
  subs: Subscription;

  constructor(
    private mainStorage: MainStorageService,
    private store: Store<fromRoot.State>
  ) { }

  ngOnInit() {
    this.initForm();
    this.ptEventsList = this.mainStorage.getPTEvents();
  }

  ngOnChanges(changes: SimpleChanges): void {
    const v = changes.id.currentValue;
    if (!v || !Number.isInteger(v) ) {
      return;
    }
    this.store.select(fromRoot.getOurSites)
      .map(sites => sites.find(s => s.id === this.id))
      .subscribe(r => this.siteState = r);
  }

  initForm() {
    this.ptEventsForm = new FormGroup({});
  }

  onClickPTEvent(event) {
    this.selectedPTEvent = event;
  }

  onCloseForm() {
    this.close.emit(true);
  }

  isPTEventExist(id: number) {
    if (!this.siteState || !this.siteState.ptEventParamsData) {return false; }
    return this.siteState.ptEventParamsData.find(item => item.id === id);
  }

  onAddEvent(value: PTEventData) {
    const mapToPTEventParamsData = {
      ...value,
      ...this.selectedPTEvent
    };
    // TODO
    // add checkbox to filled event
    // update site with filled pt-event
    this.store.dispatch(new UserActions.DoSendPixelTrackingEventParams(
      {
        id: this.id,
        ptEventParamsData: mapToPTEventParamsData
      })
    );
    this.selectedPTEvent = null;
  }

}
